package com.betraven.dao;

import com.betraven.domain.Result;
import org.springframework.data.repository.CrudRepository;


public interface ResultRepository extends CrudRepository<Result, Long> {

}
