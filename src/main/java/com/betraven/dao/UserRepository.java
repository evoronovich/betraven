package com.betraven.dao;

import com.betraven.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    User findByUsername(String username);

}
