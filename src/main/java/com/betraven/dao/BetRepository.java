package com.betraven.dao;

import com.betraven.domain.Bet;
import com.betraven.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BetRepository extends PagingAndSortingRepository<Bet, Long> {

    Page<Bet> findAllByUser(User user, Pageable pageable);
}
