package com.betraven.dao;

import com.betraven.domain.Team;
import org.springframework.data.repository.CrudRepository;


public interface TeamRepository extends CrudRepository<Team, Long> {

    Team findByName(String name);

}
