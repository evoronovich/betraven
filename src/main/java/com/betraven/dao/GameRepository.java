package com.betraven.dao;

import com.betraven.domain.Game;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface GameRepository extends PagingAndSortingRepository<Game, Long> {

    Page<Game> findByResultIsNullAndGameTimeBefore(Date date, Pageable pageable);

    List<Game> findByResultCachedIsFalse();

    Page<Game> findByGameTimeAfter(Date date, Pageable pageable);

}
