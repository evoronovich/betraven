package com.betraven.dao;

import com.betraven.domain.Ratio;
import com.betraven.domain.Team;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface RatioRepository extends PagingAndSortingRepository<Ratio, Long> {

}
