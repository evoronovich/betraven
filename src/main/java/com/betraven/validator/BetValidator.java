package com.betraven.validator;

import com.betraven.domain.Account;
import com.betraven.domain.Bet;
import com.betraven.service.MoneyService;
import com.betraven.service.UserAccountService;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class BetValidator implements Validator {

    UserAccountService userAccountService;
    MoneyService moneyService;

    @Autowired
    public BetValidator(UserAccountService userAccountService,
        MoneyService moneyService) {
        this.userAccountService = userAccountService;
        this.moneyService = moneyService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Bet.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        if (errors.hasErrors()) {
            return;
        }
        Bet bet = (Bet) o;
        BigDecimal moneytoBet = bet.getBetAmount();
        Account userAccount = userAccountService.findOne(bet.getUser().getUserId()).getAccount();
        if (!moneyService.isEnoughFunds(userAccount, moneytoBet)) {
            errors.rejectValue("betAmount", "money.not_enough");
        }
    }
}
