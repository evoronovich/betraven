package com.betraven.validator;

import com.betraven.domain.Account;
import com.betraven.service.MoneyService;
import com.betraven.service.UserAccountService;
import com.betraven.web.dto.MoneyForm;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AccountValidator implements Validator {

    UserAccountService userAccountService;
    MoneyService moneyService;

    @Autowired
    public AccountValidator(UserAccountService userAccountService,
        MoneyService moneyService) {
        this.userAccountService = userAccountService;
        this.moneyService = moneyService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return MoneyForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        if (errors.hasErrors()) {
            return;
        }
        MoneyForm moneyForm = (MoneyForm) o;
        BigDecimal moneyToCash = moneyForm.getMoney();
        Account officeAccount = userAccountService.getCompanyAccount();
        Account userAccount = userAccountService.findOne(moneyForm.getUser().getUserId())
            .getAccount();
        if (!moneyService.isEnoughFunds(userAccount, moneyToCash) || !moneyService
            .isEnoughFunds(officeAccount, moneyToCash)) {
            errors.rejectValue("money", "money.not_enough");
        }
    }
}
