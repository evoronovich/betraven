package com.betraven.validator;

import com.betraven.domain.User;
import com.betraven.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class UserValidator implements Validator {

    private UserAccountService userAccountService;

    @Autowired
    public UserValidator(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        if (userAccountService.findByUserName(user.getUsername()) != null) {
            errors.rejectValue("username", "user.username.duplicate");
        }
        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "user.password.password_not_match");
        }
    }

}
