package com.betraven.validator;

import com.betraven.domain.Team;
import com.betraven.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class TeamValidator implements Validator {

    private TeamService teamService;

    @Autowired
    public TeamValidator(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Team.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Team team = (Team) o;
        if (teamService.findTeamByName(team.getName()) != null) {
            errors.rejectValue("name", "team.name.duplicate");
        }
    }
}
