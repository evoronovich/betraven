package com.betraven.validator;

import com.betraven.domain.Game;
import com.betraven.domain.Ratio;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class GameValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Game.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Game game = (Game) o;
        List<Ratio> ratioList = game.getRatios();
        if (ratioList.get(0).getTeam().equals(ratioList.get(1).getTeam())) {
            errors.reject("admin.match.team_duplicate");
        }

    }

    public Integer validatePage(Integer page){
        if (page == null || page < 0) {
            page = 0;
        }
        return page;
    }
}
