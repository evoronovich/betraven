package com.betraven.service;

import com.betraven.domain.Account;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class MoneyServiceImpl implements MoneyService {

    private UserAccountService userAccountService;

    @Autowired
    public MoneyServiceImpl(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @Override
    public void addMoney(Account userAccount, BigDecimal moneyToAdd) {

        Account officeAccount = userAccountService.getCompanyAccount();

        BigDecimal userMoney = userAccount.getMoney();
        BigDecimal officeMoney = officeAccount.getMoney();

        userMoney = userMoney.add(moneyToAdd);
        officeMoney = officeMoney.add(moneyToAdd);

        userAccount.setMoney(userMoney);
        officeAccount.setMoney(officeMoney);

        userAccountService.saveAccount(userAccount);
    }

    @Override
    public boolean isEnoughFunds(Account account, BigDecimal money) {
        return account.getMoney() != null && account.getMoney().compareTo(money) >= 0;
    }

    @Override
    public void cashAccount(Account userAccount, BigDecimal moneyToCash) {
        Account officeAccount = userAccountService.getCompanyAccount();
        BigDecimal userMoney = userAccount.getMoney();
        BigDecimal officeMoney = officeAccount.getMoney();
        userMoney = userMoney.add(moneyToCash.negate());
        officeMoney = officeMoney.add(moneyToCash.negate());
        officeAccount.setMoney(officeMoney);
        userAccount.setMoney(userMoney);
        userAccountService.saveAccount(userAccount);
    }

}
