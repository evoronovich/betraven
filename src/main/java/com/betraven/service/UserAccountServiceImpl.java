package com.betraven.service;

import com.betraven.dao.AccountRepository;
import com.betraven.dao.RoleRepository;
import com.betraven.dao.UserRepository;
import com.betraven.domain.Account;
import com.betraven.domain.Role;
import com.betraven.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserAccountServiceImpl implements UserAccountService {

    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private RoleRepository roleRepository;
    private AccountRepository accountRepository;
    private static final String COMPANY_NAME = "Company";
    private static final String ROLE_USER = "ROLE_USER";


    @Autowired
    public UserAccountServiceImpl(UserRepository userRepository,
        BCryptPasswordEncoder bCryptPasswordEncoder, RoleRepository roleRepository,
        AccountRepository accountRepository) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleRepository = roleRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public User saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role role = roleRepository.findByName(ROLE_USER);
        Set<Role> set = new HashSet<>();
        set.add(role);
        user.setRoles(set);
        return userRepository.save(user);
    }

    @Override
    public void setUserAdmin(User user) {
        User userFromBase = userRepository.findByUsername(user.getUsername());
        userFromBase.setRoles(user.getRoles());
        userRepository.save(userFromBase);
    }

    @Override
    public User findByUserName(String name) {
        return userRepository.findByUsername(name);
    }

    @Override
    public User findOne(Long userId) {
        return userRepository.findOne(userId);
    }

    @Override
    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Account getCompanyAccount() {
        return findByUserName(COMPANY_NAME).getAccount();
    }

    @Override
    public List<Role> getRoles() {
        return (List) roleRepository.findAll();
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

}
