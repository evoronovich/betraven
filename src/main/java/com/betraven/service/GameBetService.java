package com.betraven.service;

import com.betraven.domain.Bet;
import com.betraven.domain.Game;
import com.betraven.domain.Ratio;
import com.betraven.domain.Result;
import com.betraven.domain.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface GameBetService {


    Game getGame(Long id);

    Bet saveBet(User user, Bet bet);

    Game saveGame(Game game);

    Page<Game> getGamesWithoutResult(Pageable pageable);

    Page<Game> getFutureGames(Pageable pageable);

    Page<Game> getAllGames(Pageable pageable);

    Ratio getRatio(Long id);

    Result saveResult(Result result);

    List<Ratio> calculateRatios(List<Ratio> ratios);

    void cashBets();

    void deleteGame(Long gameId);

    Page<Bet> getAllBetsByUser(User user, Pageable pageable);
}
