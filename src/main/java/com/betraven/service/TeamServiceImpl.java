package com.betraven.service;

import com.betraven.dao.RatioRepository;
import com.betraven.dao.TeamRepository;
import com.betraven.domain.Game;
import com.betraven.domain.Ratio;
import com.betraven.domain.Team;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TeamServiceImpl implements TeamService {

    TeamRepository teamRepository;
    RatioRepository ratioRepository;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, RatioRepository ratioRepository) {
        this.teamRepository = teamRepository;
        this.ratioRepository = ratioRepository;
    }

    @Override
    public Team findTeamById(Long id) {
        return teamRepository.findOne(id);
    }

    @Override
    public void saveTeam(Team team) {
        teamRepository.save(team);
    }

    @Override
    public void deleteTeam(Team team) {
        teamRepository.delete(team.getTeamId());
    }

    @Override
    public List<Team> getAllTeams() {
        return (List) teamRepository.findAll();
    }

    @Override
    public List<Team> getTeamsByGame(Game game) {
        List<Team> teams = new ArrayList<>();
        for (Ratio ratio : game.getRatios()) {
            teams.add(ratio.getTeam());
        }
        return teams;
    }

    @Override
    public Team findTeamByName(String name) {
        return teamRepository.findByName(name);
    }


}
