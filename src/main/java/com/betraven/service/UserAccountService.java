package com.betraven.service;


import com.betraven.domain.Account;
import com.betraven.domain.Role;
import com.betraven.domain.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface UserAccountService {

    User saveUser(User user);

    void setUserAdmin(User user);

    User findByUserName(String name);

    User findOne(Long userId);

    Account saveAccount(Account account);

    Account getCompanyAccount();

    List<Role> getRoles();

    Page<User> findAll(Pageable pageable);
}
