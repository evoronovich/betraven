package com.betraven.service;

import com.betraven.domain.Game;
import com.betraven.domain.Team;
import java.util.List;


public interface TeamService {

    Team findTeamById(Long id);

    void saveTeam(Team team);

    List<Team> getAllTeams();

    List<Team> getTeamsByGame(Game game);

    Team findTeamByName(String name);

    void deleteTeam(Team team);

}
