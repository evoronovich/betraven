package com.betraven.service;

import com.betraven.dao.*;
import com.betraven.domain.*;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;


@Transactional
@Service
public class GameBetServiceImpl implements GameBetService {

    private static final double PROFIT_COMPANY = 0.95;
    BetRepository betRepository;
    GameRepository gameRepository;
    RatioRepository ratioRepository;
    ResultRepository resultRepository;
    UserAccountService userAccountService;

    @Autowired
    public GameBetServiceImpl(BetRepository betRepository,
        GameRepository gameRepository,
        RatioRepository ratioRepository, ResultRepository resultRepository,
        UserAccountService userAccountService) {
        this.betRepository = betRepository;
        this.gameRepository = gameRepository;
        this.ratioRepository = ratioRepository;
        this.resultRepository = resultRepository;
        this.userAccountService = userAccountService;
    }


    @Override
    public Game getGame(Long id) {
        return gameRepository.findOne(id);
    }

    @Override
    public Bet saveBet(User user, Bet bet) {
        User userFromBase = userAccountService.findOne(user.getUserId());
        Account userAccount = userFromBase.getAccount();
        BigDecimal userMoney = userAccount.getMoney();
        userMoney = userMoney.add(bet.getBetAmount().negate());
        userAccount.setMoney(userMoney);
        List<Bet> bets = userFromBase.getBets();
        bets.add(bet);
        userFromBase.setBets(bets);
        bet.setUser(userFromBase);
        return betRepository.save(bet);
    }

    @Override
    public Game saveGame(Game game) {

        return gameRepository.save(game);
    }

    @Override
    public Page<Game> getFutureGames(Pageable pageable) {
        Date date = new Date();
        return gameRepository.findByGameTimeAfter(date, pageable);
    }

    @Override
    public Page<Game> getAllGames(Pageable pageable) {
        return gameRepository.findAll(pageable);
    }

    @Override
    public Page<Game> getGamesWithoutResult(Pageable pageable) {
        Date date = new Date();
        return gameRepository.findByResultIsNullAndGameTimeBefore(date, pageable);
    }


    @Override
    public Ratio getRatio(Long id) {
        return ratioRepository.findOne(id);
    }

    @Override
    public Result saveResult(Result result) {
        return resultRepository.save(result);
    }


    @Transactional
    @Scheduled(fixedRate = 3600000)
    @Override
    public void cashBets() {
        List<Game> games = gameRepository.findByResultCachedIsFalse();
        Account companyAccount = userAccountService.getCompanyAccount();
        BigDecimal officeMoney = companyAccount.getMoney();
        for (Game game : games) {
            for (Bet bet : game.getBets()) {
                BigDecimal userMoney = bet.getUser().getAccount().getMoney();
                if (bet.getRatio().getTeam().equals(game.getResult().getWinner())) {
                    userMoney = userMoney
                        .add(bet.getBetAmount().multiply(bet.getRatio().getValue()));
                } else {
                    officeMoney = officeMoney.add(bet.getBetAmount());
                }
                bet.getUser().getAccount().setMoney(userMoney);


            }
            game.getResult().setCached(true);
        }
        companyAccount.setMoney(officeMoney);

    }

    @Override
    public void deleteGame(Long gameId) {
        gameRepository.delete(gameId);
    }

    @Override
    public Page<Bet> getAllBetsByUser(User user, Pageable pageable) {
        return betRepository.findAllByUser(user, pageable);
    }


    @Override
    public List<Ratio> calculateRatios(List<Ratio> ratios) {
        int ratingOfTwoTeams = 0;
        for (Ratio ratio : ratios) {
            ratingOfTwoTeams = ratingOfTwoTeams + ratio.getTeam().getRating().getValue();
        }
        for (int i = 0; i < ratios.size(); i++) {
            Integer teamRating = ratios.get(i).getTeam().getRating().getValue();
            BigDecimal ratioValue = BigDecimal
                .valueOf(PROFIT_COMPANY * ratingOfTwoTeams / teamRating)
                .setScale(2, BigDecimal.ROUND_HALF_DOWN);
            ratios.get(i).setValue(ratioValue);
        }
        return ratios;
    }


}
