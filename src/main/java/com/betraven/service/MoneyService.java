package com.betraven.service;

import com.betraven.domain.Account;
import java.math.BigDecimal;


public interface MoneyService {

    void addMoney(Account userAccount, BigDecimal moneyToAdd);

    boolean isEnoughFunds(Account account, BigDecimal money);

    void cashAccount(Account userAccount, BigDecimal moneyToCash);

}
