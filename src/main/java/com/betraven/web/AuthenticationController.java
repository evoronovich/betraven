package com.betraven.web;

import com.betraven.domain.Account;
import com.betraven.domain.User;
import com.betraven.service.UserAccountService;
import com.betraven.service.SecurityService;
import com.betraven.validator.UserValidator;
import java.math.BigDecimal;
import java.util.Locale;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class AuthenticationController {

    private UserAccountService userAccountService;
    private UserValidator userValidator;
    private SecurityService securityService;
    private MessageSource messageSource;
    private static final String LOGIN_PAGE = "login";
    private static final String REGISTRATION_PAGE = "registration";
    private static final String WELCOME_PAGE = "welcome";

    @Autowired
    public AuthenticationController(UserAccountService userAccountService,
        UserValidator userValidator, SecurityService securityService, MessageSource messageSource) {
        this.userAccountService = userAccountService;
        this.userValidator = userValidator;
        this.securityService = securityService;
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout, Locale locale) {
        if (error != null) {
            model.addAttribute("error",
                messageSource.getMessage("user.password.invalid", null, locale));
        }

        if (logout != null) {
            model
                .addAttribute("message",
                    messageSource.getMessage("user.logout.success", null, locale));
        }
        return LOGIN_PAGE;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return REGISTRATION_PAGE;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@Valid @ModelAttribute("userForm") User userForm,
        BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return REGISTRATION_PAGE;
        }
        Account account = new Account();
        account.setMoney(new BigDecimal(0));
        userForm.setAccount(account);
        userAccountService.saveUser(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }


    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcome() {
        return WELCOME_PAGE;
    }

}
