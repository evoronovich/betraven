package com.betraven.web.dto;

import com.betraven.domain.User;
import java.math.BigDecimal;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class MoneyForm {

    private BigDecimal money;
    private User user;

    @Max(value = 1000, message = "{typeMismatch.java.math.BigDecimal}")
    @Min(value = 1, message = "{typeMismatch.java.math.BigDecimal}")
    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
