package com.betraven.web;


import com.betraven.domain.*;
import com.betraven.service.GameBetService;
import com.betraven.service.MoneyService;
import com.betraven.service.TeamService;
import com.betraven.service.UserAccountService;
import com.betraven.validator.AccountValidator;
import com.betraven.validator.BetValidator;
import com.betraven.validator.GameValidator;
import com.betraven.web.dto.MoneyForm;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@SessionAttributes(types = {User.class})
public class BetRavenController {

    private GameBetService gameBetService;
    private MoneyService moneyService;
    private UserAccountService userAccountService;
    private TeamService teamService;
    private GameValidator gameValidator;
    private AccountValidator accountValidator;
    private BetValidator betValidator;
    private static final int PAGE_SIZE = 5;
    private static final String MAX_PAGES = "maxPages";
    private static final String SUCCESS_PAGE = "success";
    private static final String HOME_PAGE = "home";
    private static final String GAMES_PAGE = "games";
    private static final String ADD_CASH_MONEY_PAGE = "addCashMoney";
    private static final String MAKE_BET_PAGE = "makeBet";
    private static final String CHOOSE_TEAM_PAGE = "chooseTeam";
    private static final String SHOW_HISTORY_PAGE = "showHistory";
    private static final String PROFILE_PAGE = "profile";


    @Autowired
    public BetRavenController(GameBetService gameBetService,
        MoneyService moneyService, UserAccountService userAccountService, TeamService teamService,
        GameValidator gameValidator, AccountValidator accountValidator, BetValidator betValidator) {
        this.gameBetService = gameBetService;
        this.moneyService = moneyService;
        this.userAccountService = userAccountService;
        this.teamService = teamService;
        this.gameValidator = gameValidator;
        this.accountValidator = accountValidator;
        this.betValidator = betValidator;
    }


    @RequestMapping(value = {"home", "/"}, method = RequestMethod.GET)
    public String home(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        if (!name.equals("anonymousUser") && !model.containsAttribute("user")) {
            model.addAttribute(userAccountService.findByUserName(name));
        }
        return HOME_PAGE;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(Model model, @ModelAttribute("user") User user,
        @RequestParam(required = false) Integer page) {

        page = gameValidator.validatePage(page);
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, Direction.DESC, "game.gameTime");
        Page<Bet> betPage = gameBetService.getAllBetsByUser(user, pageRequest);
        model.addAttribute("page", page);
        model.addAttribute("bets", betPage.getContent());
        model.addAttribute(MAX_PAGES, betPage.getTotalPages());
        model.addAttribute("user", userAccountService.findOne(user.getUserId()));
        return PROFILE_PAGE;
    }

    @RequestMapping(value = "/addCashMoney", method = RequestMethod.GET)
    public String addCashMoney() {
        return ADD_CASH_MONEY_PAGE;
    }

    @RequestMapping(value = "/addMoney", method = {RequestMethod.POST, RequestMethod.GET})
    public String addMoney(@ModelAttribute("user") User user,
        Model model, @Valid @ModelAttribute("moneyForm") MoneyForm moneyForm,
        BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes
                .addFlashAttribute("org.springframework.validation.BindingResult.moneyForm",
                    bindingResult);
            redirectAttributes.addFlashAttribute("moneyForm", moneyForm);
            return "redirect:/" + ADD_CASH_MONEY_PAGE;
        }
        Account userAccount = userAccountService.findByUserName(user.getUsername()).getAccount();
        moneyService.addMoney(userAccount, moneyForm.getMoney());
        model.addAttribute("user", userAccountService.findOne(user.getUserId()));
        return SUCCESS_PAGE;
    }

    @RequestMapping(value = "/cashMoney", method = {RequestMethod.POST, RequestMethod.GET})
    public String cashMoney(@ModelAttribute("user") User user, Model model,
        @Valid @ModelAttribute("moneyForm") MoneyForm moneyForm, BindingResult bindingResult,
        RedirectAttributes redirectAttributes) {
        moneyForm.setUser(user);
        accountValidator.validate(moneyForm, bindingResult);
        if (bindingResult.hasErrors()) {
            redirectAttributes
                .addFlashAttribute("org.springframework.validation.BindingResult.moneyForm",
                    bindingResult);
            redirectAttributes.addFlashAttribute("moneyForm", moneyForm);
            return "redirect:/" + ADD_CASH_MONEY_PAGE;
        }
        Account userAccount = userAccountService.findByUserName(user.getUsername()).getAccount();
        BigDecimal moneyToCash = moneyForm.getMoney();
        moneyService.cashAccount(userAccount, moneyToCash);
        model.addAttribute("user", userAccountService.findOne(user.getUserId()));
        return SUCCESS_PAGE;
    }

    @RequestMapping(value = {"/games"}, method = RequestMethod.GET)
    public String showGames(Model model, @RequestParam(required = false) Integer page) {
        page = gameValidator.validatePage(page);
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, Direction.ASC, "gameTime");
        Page<Game> gamePage = gameBetService.getFutureGames(pageRequest);
        model.addAttribute("page", page);
        model.addAttribute("games", gamePage.getContent());
        model.addAttribute(MAX_PAGES, gamePage.getTotalPages());

        return GAMES_PAGE;
    }

    @RequestMapping(value = "/makeBet", method = RequestMethod.GET)
    public String makeBet(Model model, @RequestParam Long ratioId, @RequestParam Long gameId) {
        Bet bet = new Bet();
        bet.setRatio(gameBetService.getRatio(ratioId));
        bet.setGame(gameBetService.getGame(gameId));
        model.addAttribute("bet", bet);
        return MAKE_BET_PAGE;
    }

    @RequestMapping(value = "/makeBet", method = RequestMethod.POST)
    public String makeBet(@ModelAttribute("user") User user, Model model,
        @Valid @ModelAttribute("bet") Bet bet, BindingResult bindingResult) {
        bet.setUser(user);
        betValidator.validate(bet, bindingResult);
        if (bindingResult.hasErrors()) {
            return MAKE_BET_PAGE;
        }
        gameBetService.saveBet(user, bet);
        model.addAttribute("user", userAccountService.findOne(user.getUserId()));
        return SUCCESS_PAGE;

    }

    @RequestMapping(value = "/chooseTeam", method = RequestMethod.GET)
    public String chooseTeam(Model model) {
        Team team = new Team();
        model.addAttribute("team", team);
        model.addAttribute("teams", teamService.getAllTeams());
        return CHOOSE_TEAM_PAGE;
    }

    @RequestMapping(value = "/showHistory", method = RequestMethod.GET)
    public String showHistory(Model model, @ModelAttribute("team") Team team,
        @RequestParam(required = false) Integer page) {
        List<Ratio> ratios = teamService.findTeamById(team.getTeamId()).getRatios();
        List<Game> games = new ArrayList<>();
        ratios.forEach(ratio -> games.add(ratio.getGame()));
        PagedListHolder<Game> gamePagedListHolder = new PagedListHolder<>();
        gamePagedListHolder.setPageSize(PAGE_SIZE);
        gamePagedListHolder.setSource(games);
        page = gameValidator.validatePage(page);
        gamePagedListHolder.setPage(page);
        List<Game> pageList = gamePagedListHolder.getPageList();
        model.addAttribute("page", page);
        model.addAttribute("games", pageList);
        model.addAttribute(MAX_PAGES, gamePagedListHolder.getPageCount());
        model.addAttribute("team", team);
        return SHOW_HISTORY_PAGE;
    }

    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public String success() {
        return SUCCESS_PAGE;
    }

    @ModelAttribute
    public MoneyForm addMoneyFrom() {
        return new MoneyForm();
    }

}
