package com.betraven.web;


import com.betraven.domain.Game;
import com.betraven.domain.Rating;
import com.betraven.domain.Ratio;
import com.betraven.domain.Result;
import com.betraven.domain.Team;
import com.betraven.domain.User;
import com.betraven.service.GameBetService;
import com.betraven.service.TeamService;
import com.betraven.service.UserAccountService;
import com.betraven.validator.GameValidator;
import com.betraven.validator.TeamValidator;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes(types = User.class)
public class AdminController {

    private GameBetService gameBetService;
    private UserAccountService userAccountService;
    private TeamService teamService;
    private TeamValidator teamValidator;
    private GameValidator gameValidator;
    private static final int PAGE_SIZE = 5;
    private static final String TEAMS = "teams";
    private static final String MAX_PAGES = "maxPages";
    private static final String ADMIN_HOME_PAGE = "adminHome";
    private static final String CHOOSE_MATCH_PAGE = "chooseMatch";
    private static final String SET_RESULTS_PAGE = "setResults";
    private static final String CREATE_MATCH_PAGE = "createMatch";
    private static final String DELETE_MATCH_PAGE = "deleteMatch";
    private static final String CREATE_TEAM_PAGE = "createTeam";
    private static final String DELETE_TEAM_PAGE = "deleteTeam";
    private static final String CHOOSE_USER_PAGE = "chooseUser";
    private static final String SET_ADMIN_PAGE = "setAdmin";
    private static final String SUCCESS_PAGE = "success";

    @Autowired
    public AdminController(GameBetService gameBetService,
        UserAccountService userAccountService, TeamService teamService,
        TeamValidator teamValidator, GameValidator gameValidator) {
        this.gameBetService = gameBetService;
        this.userAccountService = userAccountService;
        this.teamService = teamService;
        this.teamValidator = teamValidator;
        this.gameValidator = gameValidator;
    }

    @RequestMapping(value = {"/adminHome"}, method = RequestMethod.GET)
    public String adminHome(Model model) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!name.equals("anonymousUser") && !model.containsAttribute("user")) {
            model.addAttribute(userAccountService.findByUserName(name));
        }
        return ADMIN_HOME_PAGE;
    }

    @RequestMapping(value = "/createMatch", method = RequestMethod.GET)
    public String createMatch(Model model) {
        Game game = new Game();
        model.addAttribute("gameForm", game);
        model.addAttribute(TEAMS, teamService.getAllTeams());
        return CREATE_MATCH_PAGE;
    }

    @RequestMapping(value = "/createMatch", method = RequestMethod.POST)
    public String createMatch(@Valid @ModelAttribute("gameForm") Game gameForm,
        BindingResult bindingResult, Model model) {
        gameValidator.validate(gameForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute(TEAMS, teamService.getAllTeams());
            return CREATE_MATCH_PAGE;
        }
        gameBetService.saveGame(gameForm);

        return SUCCESS_PAGE;
    }

    @RequestMapping(value = "/setAutoRatio", method = RequestMethod.GET)
    public String setAutoRatio(@ModelAttribute("gameForm") Game gameForm, Model model) {
        List<Team> teams = new ArrayList<>();
        for (Ratio ratio : gameForm.getRatios()) {
            teams.add(ratio.getTeam());
        }
        gameForm.setRatios(gameBetService.calculateRatios(gameForm.getRatios()));
        model.addAttribute("gameForm", gameForm);
        model.addAttribute(TEAMS, teams);
        return CREATE_MATCH_PAGE;
    }

    @RequestMapping(value = "/chooseMatch", method = RequestMethod.GET)
    public String chooseGame(Model model, @RequestParam(required = false) Integer page) {
        page = gameValidator.validatePage(page);
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE);
        Page<Game> gamePage = gameBetService.getGamesWithoutResult(pageRequest);
        model.addAttribute("page", page);
        model.addAttribute("games", gamePage.getContent());
        model.addAttribute(MAX_PAGES, gamePage.getTotalPages());
        return CHOOSE_MATCH_PAGE;
    }

    @RequestMapping(value = "/setResults", method = RequestMethod.GET)
    public String setResult(Model model, @RequestParam Long gameId) {
        Result result = new Result();
        Game game = gameBetService.getGame(gameId);
        result.setGame(game);
        model.addAttribute(TEAMS, teamService.getTeamsByGame(game));
        model.addAttribute("result", result);
        return SET_RESULTS_PAGE;
    }

    @RequestMapping(value = "/setResults", method = RequestMethod.POST)
    public String setResult(@ModelAttribute("result") Result result) {
        if (result.getWinner() == null) {
            return "redirect:/chooseMatch";
        }
        Team winner = result.getWinner();
        Rating rating = result.getWinner().getRating();
        rating.setValue(rating.getValue() + 1);
        winner.setRating(rating);
        result.setWinner(winner);
        gameBetService.saveResult(result);
        return SUCCESS_PAGE;
    }

    @RequestMapping(value = "/chooseMatchToDelete", method = RequestMethod.GET)
    public String chooseMatchToDelete(Model model, @RequestParam(required = false) Integer page) {
        page = gameValidator.validatePage(page);
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, Direction.ASC,"gameTime");
        Page<Game> gamePage = gameBetService.getAllGames(pageRequest);
        model.addAttribute("page", page);
        model.addAttribute("games", gamePage.getContent());
        model.addAttribute(MAX_PAGES, gamePage.getTotalPages());
        return DELETE_MATCH_PAGE;
    }

    @RequestMapping(value = "/deleteMatch", method = RequestMethod.GET)
    public String deleteMatch(@RequestParam Long gameId) {
        gameBetService.deleteGame(gameId);
        return SUCCESS_PAGE;
    }

    @RequestMapping(value = "/createTeam", method = RequestMethod.GET)
    public String addTeam(Model model) {
        model.addAttribute("team", new Team());
        return CREATE_TEAM_PAGE;
    }

    @RequestMapping(value = "/createTeam", method = RequestMethod.POST)
    public String createTeam(@Valid @ModelAttribute Team team, BindingResult bindingResult) {
        teamValidator.validate(team, bindingResult);
        if (bindingResult.hasErrors()) {
            return CREATE_TEAM_PAGE;
        }
        Rating rating = new Rating();
        rating.setValue(1);
        team.setRating(rating);
        teamService.saveTeam(team);
        return SUCCESS_PAGE;
    }


    @RequestMapping(value = "/deleteTeam", method = RequestMethod.GET)
    public String deleteTeam(Model model) {
        model.addAttribute("team", new Team());
        model.addAttribute(TEAMS, teamService.getAllTeams());
        return DELETE_TEAM_PAGE;
    }

    @RequestMapping(value = "/deleteTeam", method = RequestMethod.POST)
    public String deleteTeam(@ModelAttribute Team team, BindingResult bindingResult, Model model) {
        if (teamService.findTeamById(team.getTeamId()).getRatios().isEmpty()) {
            teamService.deleteTeam(team);
        } else {
            bindingResult.rejectValue("teamId", "team.delete.not_allow");
            model.addAttribute(TEAMS, teamService.getAllTeams());
            return DELETE_TEAM_PAGE;
        }
        return SUCCESS_PAGE;
    }


    @RequestMapping(value = "/cashBets", method = RequestMethod.GET)
    public String cashBets() {
        gameBetService.cashBets();
        return SUCCESS_PAGE;
    }

    @RequestMapping(value = "/chooseUser", method = RequestMethod.GET)
    public String chooseUser(Model model, @RequestParam(required = false) Integer page) {
        page = gameValidator.validatePage(page);
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE);
        Page<User> userPage = userAccountService.findAll(pageRequest);
        model.addAttribute("page", page);
        model.addAttribute("users", userPage.getContent());
        model.addAttribute(MAX_PAGES, userPage.getTotalPages());
        return CHOOSE_USER_PAGE;
    }

    @RequestMapping(value = "/setAdmin", method = RequestMethod.GET)
    public String addAdmin(@RequestParam Long userId, Model model) {
        model.addAttribute("user", userAccountService.findOne(userId));
        model.addAttribute("roles", userAccountService.getRoles());
        return SET_ADMIN_PAGE;
    }

    @RequestMapping(value = "/setAdmin", method = RequestMethod.POST)
    public String addAdmin(@ModelAttribute User user) {
        userAccountService.setUserAdmin(user);
        return SUCCESS_PAGE;
    }

}
