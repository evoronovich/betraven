package com.betraven.domain;

import javax.persistence.*;
import java.util.List;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
public class Team {

    private Long teamId;
    private String name;
    private List<Ratio> ratios;
    private List<Result> results;
    private Rating rating;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "team_id")
    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    @Column(name = "name")
    @NotEmpty(message = "{field.required}")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "team")
    public List<Ratio> getRatios() {
        return ratios;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "winner")
    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public void setRatios(List<Ratio> ratios) {
        this.ratios = ratios;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rating_id")
    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Team team = (Team) o;

        if (teamId != null ? !teamId.equals(team.teamId) : team.teamId != null) {
            return false;
        }
        if (name != null ? !name.equals(team.name) : team.name != null) {
            return false;
        }
        return rating != null ? rating.equals(team.rating) : team.rating == null;
    }

    @Override
    public int hashCode() {
        int result = teamId != null ? teamId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        return result;
    }
}
