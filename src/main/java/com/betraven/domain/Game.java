package com.betraven.domain;

import javax.validation.Valid;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Game {

    private Long gameId;
    private Date gameTime;
    private List<Ratio> ratios;
    private Result result;
    private List<Bet> bets;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "game_id")
    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    @Column(name = "game_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")

    public Date getGameTime() {
        return gameTime;
    }

    public void setGameTime(Date gameTime) {
        this.gameTime = gameTime;
    }

    @Valid
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "game", orphanRemoval = true, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    public List<Ratio> getRatios() {
        return ratios;
    }

    public void setRatios(List<Ratio> ratios) {
        this.ratios = ratios;
        for (Ratio ratio : ratios) {
            ratio.setGame(this);
        }
    }

    @OneToOne(mappedBy = "game", cascade = CascadeType.ALL)
    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "game", orphanRemoval = true, fetch = FetchType.EAGER)
    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Game game = (Game) o;

        if (gameId != null ? !gameId.equals(game.gameId) : game.gameId != null) {
            return false;
        }
        if (gameTime != null ? !gameTime.equals(game.gameTime) : game.gameTime != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int resultHash = gameId != null ? gameId.hashCode() : 0;
        resultHash = 31 * resultHash + (gameTime != null ? gameTime.hashCode() : 0);
        return resultHash;
    }


}
