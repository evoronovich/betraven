package com.betraven.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Entity
public class Bet {

    private Long betId;
    private BigDecimal betAmount;
    private Ratio ratio;
    private User user;
    private Game game;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bet_id")
    public Long getBetId() {
        return betId;
    }

    public void setBetId(Long betId) {
        this.betId = betId;
    }

    @Column(name = "bet_amount")
    @NotNull(message = "{field.required}")
    @Min(value = 1, message = "{typeMismatch.java.math.BigDecimal}")
    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }


    @ManyToOne
    @JoinColumn(name = "ratio_id")
    public Ratio getRatio() {
        return ratio;
    }

    public void setRatio(Ratio ratio) {
        this.ratio = ratio;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "game_id")
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Bet bet = (Bet) o;

        if (betId != null ? !betId.equals(bet.betId) : bet.betId != null) {
            return false;
        }
        if (betAmount != null ? !betAmount.equals(bet.betAmount) : bet.betAmount != null) {
            return false;
        }
        if (ratio != null ? !ratio.equals(bet.ratio) : bet.ratio != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = betId != null ? betId.hashCode() : 0;
        result = 31 * result + (betAmount != null ? betAmount.hashCode() : 0);
        result = 31 * result + (ratio != null ? ratio.hashCode() : 0);
        return result;
    }
}
