package com.betraven.domain;

import java.math.BigDecimal;
import javax.persistence.*;
import java.util.List;
import javax.validation.constraints.NotNull;


@Entity
public class Ratio {

    private Long ratioId;
    private BigDecimal value;
    private Team team;
    private Game game;
    private List<Bet> bets;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ratio_id")
    public Long getRatioId() {
        return ratioId;
    }

    public void setRatioId(Long ratioId) {
        this.ratioId = ratioId;
    }

    @Column(name = "value")
    @NotNull(message = "{field.required}")
    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @ManyToOne
    @JoinColumn(name = "team_id")
    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @ManyToOne
    @JoinColumn(name = "game_id")
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ratio", orphanRemoval = true, fetch = FetchType.EAGER)
    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Ratio ratio = (Ratio) o;

        if (ratioId != null ? !ratioId.equals(ratio.ratioId) : ratio.ratioId != null) {
            return false;
        }
        if (value != null ? !value.equals(ratio.value) : ratio.value != null) {
            return false;
        }
        if (team != null ? !team.equals(ratio.team) : ratio.team != null) {
            return false;
        }
        return game != null ? game.equals(ratio.game) : ratio.game == null;
    }

    @Override
    public int hashCode() {
        int result = ratioId != null ? ratioId.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (team != null ? team.hashCode() : 0);
        result = 31 * result + (game != null ? game.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return value.toString();
    }

}
