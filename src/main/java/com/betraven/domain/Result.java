package com.betraven.domain;

import javax.persistence.*;


@Entity
public class Result {

    private Long resultId;
    private boolean cached;
    private Game game;
    private Team winner;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "result_id")
    public Long getResultId() {
        return resultId;
    }

    public void setResultId(Long resultId) {
        this.resultId = resultId;
    }

    @Column(name = "cached")
    public boolean isCached() {
        return cached;
    }

    public void setCached(boolean cached) {
        this.cached = cached;
    }


    @ManyToOne
    @JoinColumn(name = "team_id")
    public Team getWinner() {
        return winner;
    }

    public void setWinner(Team team) {
        this.winner = team;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "game_id")
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Result result = (Result) o;

        if (cached != result.cached) {
            return false;
        }
        if (resultId != null ? !resultId.equals(result.resultId) : result.resultId != null) {
            return false;
        }
        if (game != null ? !game.equals(result.game) : result.game != null) {
            return false;
        }
        return winner != null ? winner.equals(result.winner) : result.winner == null;
    }

    @Override
    public int hashCode() {
        int result = resultId != null ? resultId.hashCode() : 0;
        result = 31 * result + (cached ? 1 : 0);
        result = 31 * result + (game != null ? game.hashCode() : 0);
        result = 31 * result + (winner != null ? winner.hashCode() : 0);
        return result;
    }
}
