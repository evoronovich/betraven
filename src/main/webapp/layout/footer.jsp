<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<div class="navbar-options">
    <nav class="navbar navbar-default navbar-fixed-bottom">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="${contextPath}/home"><spring:message
                    code="bet_raven"/></a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="${contextPath}/games"><span
                    class="glyphicon glyphicon-play-circle"></span>&nbsp<spring:message
                    code="game.show_games"/></a></li>
                <li><a href="${contextPath}/chooseTeam"><span
                    class="glyphicon glyphicon-book"></span>&nbsp<spring:message
                    code="team.show_history"/></a>
                </li>
                <c:choose>
                    <c:when test="${pageContext.request.userPrincipal.name != null}">
                        <li>
                            <a href="${contextPath}/addCashMoney"><span
                                class="glyphicon glyphicon-sort"></span>&nbsp<spring:message
                                code="user.account.add_cash_money"/></a>
                        </li>
                    </c:when>
                </c:choose>
                <c:choose>
                    <c:when test="${pageContext.request.isUserInRole('ROLE_ADMIN')}">
                        <li>
                            <a href="${contextPath}/createTeam"><span
                                class="glyphicon glyphicon-plus"></span>&nbsp<spring:message
                                code="admin.team.add_new_team"/> </a>
                        </li>
                        <li>
                            <a href="${contextPath}/deleteTeam"><span
                                class="glyphicon glyphicon-minus"></span>&nbsp<spring:message
                                code="team.delete"/> </a>
                        </li>
                        <li>
                            <a href="${contextPath}/createMatch"><span
                                class="glyphicon glyphicon-ok"></span>&nbsp<spring:message
                                code="admin.match.create.new_match"/> </a>
                        </li>
                        <li>
                            <a href="${contextPath}/chooseMatchToDelete"><span
                                class="glyphicon glyphicon-remove"></span></span>
                                &nbsp<spring:message
                                    code="admin.match.delete"/> </a>
                        </li>
                        <li>
                            <a href="${contextPath}/chooseMatch"><span
                                class="glyphicon glyphicon-list-alt"></span>&nbsp<spring:message
                                code="admin.match.set_results"/> </a>
                        </li>
                        <li>
                            <a href="${contextPath}/chooseUser"><span class="glyphicon
                                                                  glyphicon-briefcase"></span>&nbsp
                                <spring:message
                                    code="admin.users"/> </a>
                        </li>
                        <li>
                            <a href="${contextPath}/cashBets"><span
                                class="glyphicon glyphicon-usd"></span>&nbsp<spring:message
                                code="admin.match.cash_bets"/> </a>
                        </li>
                    </c:when>
                </c:choose>
            </ul>
        </div>
    </nav>
</div>