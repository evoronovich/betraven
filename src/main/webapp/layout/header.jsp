<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="uri" value="javax.servlet.forward.request_uri"/>
<c:set var="URL" value="${pageContext.request.getAttribute(uri)}"/>
<c:set var="queryString" value="${pageContext.request.queryString}"></c:set>
<c:set var="lang" value="lang"/>
<html>
<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC|Andika" rel="stylesheet">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="container-fluid">
    <div class="col-md-5">
        <a href="${contextPath}/home"><img src="${contextPath}/resources/images/raven.jpg"
                                           border="0" height="150" width="450"/>
        </a>
    </div>
    <div class="col-md-2">
        <ul class="list-group">
            <c:choose>
                <c:when test="${empty queryString}">
                    <li class="list-group-item-betraven">
                        <a href="${URL}?lang=ru&">Русский</a>
                    </li>
                    <li class="list-group-item-betraven">
                        <a href="${URL}?lang=en&">English</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${queryString.contains('lang')}">
                            <li class="list-group-item-betraven">
                                <a href="${URL}?lang=ru&${queryString.substring(8)}">Русский</a>
                            </li>
                            <li class="list-group-item-betraven">
                                <a href="${URL}?lang=en&${queryString.substring(8)}">English</a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="list-group-item-betraven">
                                <a href="${URL}?lang=ru&${queryString}">Русский</a>
                            </li>
                            <li class="list-group-item-betraven">
                                <a href="${URL}?lang=en&${queryString}">English</a>
                            </li>
                        </c:otherwise>
                    </c:choose>

                </c:otherwise>
            </c:choose>
        </ul>
    </div>
    <div class="col-md-1">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
    </div>

    <ul class="nav navbar-nav navbar-right">
        <c:choose>
            <c:when test="${pageContext.request.isUserInRole('ROLE_ADMIN')}">
                <li>
                    <a href="${contextPath}/adminHome"><p><span
                        class="glyphicon glyphicon-tower"></span>&nbsp<spring:message
                        code="admin.home"/></p></a>
                </li>
            </c:when>
        </c:choose>
        <c:choose>
            <c:when test="${pageContext.request.userPrincipal.name != null}">
                <li>
                    <p class="usd-display"><span
                        class="glyphicon glyphicon-usd"></span>&nbsp ${user.account.money}</p>
                </li>
                <li>
                    <a href="${contextPath}/profile"><p><span
                        class="glyphicon glyphicon-user"></span>&nbsp${pageContext.request.userPrincipal.name}
                    </p>
                    </a>

                </li>
                <li>
                    <a class="ref" onclick="document.forms['logoutForm'].submit()"><p><span
                        class="glyphicon glyphicon-log-in"></span>&nbsp<spring:message
                        code="user.logout"/></p></a>
                </li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="${contextPath}/registration"><p><span
                        class="glyphicon glyphicon-user"></span>&nbsp<spring:message
                        code="user.registration"/></p></a>
                </li>
                <li>
                    <a href="${contextPath}/login"><p><span
                        class="glyphicon glyphicon-log-in"></span>&nbsp
                        <spring:message code="user.login"/></p></a>
                </li>
            </c:otherwise>
        </c:choose>
    </ul>
</div>
