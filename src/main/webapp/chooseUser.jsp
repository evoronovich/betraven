<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="admin.users"/></title>
</head>
<body>
<div class="container">
        <table class="table table-nonfluid text-center">
        <c:forEach items="${users}" var="user">
            <tr>
                <td>${user.username}</td>
                <td><a href="${contextPath}/setAdmin?userId=${user.userId}"><spring:message code="admin.change_role"/> </a></td>
            </tr>
        </c:forEach>
    </table>
</div>
<div class="text-center">
<ul class="pagination">
    <c:choose>
        <c:when test="${page eq 0}">
            <li class="disabled"><a onclick="return false;" href="${contextPath}/chooseUser?page=${page-1}">&lt;&lt;</a></li>
        </c:when>
        <c:otherwise>
            <li><a href="${contextPath}/chooseUser?page=${page-1}">&lt;&lt;</a></li>
        </c:otherwise>
    </c:choose>
    <c:forEach begin="1" end="${maxPages}" var="i">
        <c:choose>
            <c:when test="${page eq i-1}">
                <li class="active"><a href="${contextPath}/chooseUser?page=${i-1}">${i}</a></li>
            </c:when>
            <c:otherwise>
                <li><a href="${contextPath}/chooseUser?page=${i-1}">${i}</a></li>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <c:choose>
        <c:when test="${page eq maxPages-1}">
            <li class="disabled"><a onclick="return false;" href="${contextPath}/chooseUser?page=${page+1}">&gt;&gt;</a></li>
        </c:when>
        <c:otherwise>
            <li><a href="${contextPath}/chooseUser?page=${page+1}">&gt;&gt;</a></li>
        </c:otherwise>
    </c:choose>
</ul>
</div>
</body>
</html>
