<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="admin.match.choose_team"/> </title>
</head>
<body>
<div class="container">
    <form:form method="POST" modelAttribute="team" class="form-signin"
               action="${contextPath}/deleteTeam">
        <h2 class="form-signin-heading text-center"><spring:message code="team.delete"/></h2>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:select items="${teams}" class="form-control" itemLabel="name" path="teamId"
                         itemValue="teamId"/>
            <form:errors path="teamId" cssClass="has-error text-center"/>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
            code="team.delete"/></button>
    </form:form>
</div>
</body>
</html>
