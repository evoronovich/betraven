<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="home"/></title>
</head>
<body>
<div class="container">
    <h3 align="center"><spring:message code="home.betraven"></spring:message></h3>
    <h4 class="text-center"><a href="${contextPath}/games"><spring:message
        code="game.show_games"/></a>
    </h4>
    <h4 class="text-center"><a href="${contextPath}/chooseTeam"><spring:message
        code="team.show_history"/></a></h4>
    <c:choose>
        <c:when test="${pageContext.request.userPrincipal.name != null}">
            <h4 class="text-center"><a href="${contextPath}/addCashMoney"><spring:message
                code="user.account.add_cash_money"/></a></h4>
            <h4 class="text-center">
                <a class="ref" onclick="document.forms['logoutForm'].submit()">
                    <spring:message code="user.logout"/>
                </a>
            </h4>
            <h4 class="text-center">
                <a href="${contextPath}/profile">
                        ${pageContext.request.userPrincipal.name}
                </a>
            </h4>
        </c:when>
        <c:otherwise>
            <h4 class="text-center"><a href="${contextPath}/login"><spring:message code="user.login"/> </a></h4>
            <h4 class="text-center"><a href="${contextPath}/registration"><spring:message
                code="user.registration"/> </a></h4>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>