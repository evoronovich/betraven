<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="game.choose"/> </title>
</head>
<body>
<table class="table text-center table-nonfluid">
    <c:forEach items="${games}" var="game">
        <tr>
            <c:forEach items="${game.ratios}" var="ratio">
                <td>${ratio.team} &nbsp; [${ratio}]</td>
            </c:forEach>
            <td><fmt:formatDate value="${game.gameTime}"
                                pattern="dd.MM HH:mm"></fmt:formatDate></td>
            <td><a href="${contextPath}/deleteMatch?gameId=${game.gameId}"><spring:message code="admin.match.delete"/></a></td>
        </tr>
    </c:forEach>
</table>
<div class="text-center">
    <ul class="pagination">
        <c:choose>
            <c:when test="${page eq 0}">
                <li class="disabled"><a onclick="return false;" href="${contextPath}/chooseMatchToDelete?page=${page-1}">&lt;&lt;</a></li>
            </c:when>
            <c:otherwise>
                <li><a href="${contextPath}/chooseMatchToDelete?page=${page-1}">&lt;&lt;</a></li>
            </c:otherwise>
        </c:choose>
        <c:forEach begin="1" end="${maxPages}" var="i">
            <c:choose>
                <c:when test="${page eq i-1}">
                    <li class="active"><a href="${contextPath}/chooseMatchToDelete?page=${i-1}">${i}</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="${contextPath}/chooseMatchToDelete?page=${i-1}">${i}</a></li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:choose>
            <c:when test="${page eq maxPages-1}">
                <li class="disabled"><a onclick="return false;" href="${contextPath}/chooseMatchToDelete?page=${page+1}">&gt;&gt;</a></li>
            </c:when>
            <c:otherwise>
                <li><a href="${contextPath}/chooseMatchToDelete?page=${page+1}">&gt;&gt;</a></li>
            </c:otherwise>
        </c:choose>
    </ul>
</div>
</body>
</html>
