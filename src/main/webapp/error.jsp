<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="warning"/></title>
</head>
<body>
<h3 class="text-center"><spring:message code="error"/></h3>
</body>
</html>
