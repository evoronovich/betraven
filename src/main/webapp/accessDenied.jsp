<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="warning"/></title>
</head>
<body>
<h3><spring:message code="access.denied"/></h3>
</body>
</html>
