<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="user.registration"/></title>
</head>
<body>
<div class="container">
    <form:form method="POST" modelAttribute="userForm" class="form-signin">
        <h2 class="form-signin-heading"><spring:message code="user.account.create"/></h2>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:input type="text" path="username" class="form-control" placeholder="username"
                        autofocus="true"></form:input>
            <form:errors path="username" cssClass="has-error"></form:errors>
        </div>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:input type="text" path="email" class="form-control" placeholder="email"
                        autofocus="true"></form:input>
            <form:errors path="email" cssClass="has-error"></form:errors>
        </div>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:input type="password" path="password" class="form-control"
                        placeholder="password"></form:input>
            <form:errors path="password" cssClass="has-error"></form:errors>
        </div>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:input type="password" path="passwordConfirm" class="form-control"
                        placeholder="Confirm your password"></form:input>
            <form:errors path="passwordConfirm" cssClass="has-error"></form:errors>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
            code="submit"/></button>
    </form:form>
</div>
</body>
</html>
