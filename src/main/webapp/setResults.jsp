<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="admin.match.set_results"/></title>
</head>
<body>
<div class="container">
    <form:form method="POST" modelAttribute="result" class="form-signin"
               action="${contextPath}/setResults">
        <h2 class="form-signin-heading"><spring:message code="admin.match.set_winner"/></h2>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:radiobuttons element="span class='radio-betraven'"  path="winner" items="${teams}" itemLabel="name"/>
            <fmt:formatDate value="${result.game.gameTime}" pattern="dd.MM HH:mm"></fmt:formatDate>
            <form:hidden path="game" id="game"/>
        </div>
        <div class="form-group ${status.error ? 'has-error' : ''}"/>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
            code="admin.match.set_results"/></button>
    </form:form>
</div>
</body>
</html>
