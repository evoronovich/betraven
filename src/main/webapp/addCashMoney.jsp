<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="user.account.add_cash_money"/></title>
</head>
<body>
<div class="container">
            <form:form method="POST" modelAttribute="moneyForm" class="form-signin"
                       action="${contextPath}/addMoney">
                <form:input type="number" min="0" max="1000" step="0.01" path="money" cssClass="form-control"
                            placeholder="Money" autofocus="true" required="true"></form:input>
                <form:errors path="money" cssClass="has-error"/>
                <button class="btn btn-md btn-primary btn-block" formaction="${contextPath}/addMoney"
                        type="submit"><spring:message code="money.add"/></button>
                <button class="btn btn-md btn-primary btn-block" formaction="${contextPath}/cashMoney"
                        type="submit"><spring:message code="money.cash"/></button>
            </form:form>
</div>
</body>
</html>
