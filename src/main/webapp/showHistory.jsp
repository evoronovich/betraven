<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="team.show_history"/></title>
</head>
<body>
<table class="table text-center table-nonfluid">
    <thead>
    <tr>
        <th><spring:message code="game"/></th>
        <th><spring:message code="game.time"/></th>
        <th><spring:message code="game.winner"/></th>
    </tr>
    </thead>
    <c:forEach items="${games}" var="game">
        <tr>
            <td>
                <c:forEach items="${game.ratios}" var="ratio">
                    ${ratio.team}&nbsp[${ratio.value}] &nbsp&nbsp&nbsp
                </c:forEach>
            </td>
            <td>
                <fmt:formatDate value="${game.gameTime}" pattern="dd.MM HH:mm"></fmt:formatDate>
            </td>
            <td>
                    <span class="result-win"> ${game.result.winner}</span>
            </td>
        </tr>
    </c:forEach>
</table>
    <div class="text-center">
        <ul class="pagination">
            <c:choose>
                <c:when test="${page eq 0}">
                    <li class="disabled"><a onclick="return false;" href="${contextPath}/showHistory?page=${page-1}&teamId=${team.teamId}">&lt;&lt;</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="${contextPath}/showHistory?page=${page-1}&teamId=${team.teamId}">&lt;&lt;</a></li>
                </c:otherwise>
            </c:choose>
            <c:forEach begin="1" end="${maxPages}" var="i">
                <c:choose>
                    <c:when test="${page eq i-1}">
                        <li class="active"><a href="${contextPath}/showHistory?page=${i-1}&teamId=${team.teamId}">${i}</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="${contextPath}/showHistory?page=${i-1}&teamId=${team.teamId}">${i}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:choose>
                <c:when test="${page eq maxPages-1}">
                    <li class="disabled"><a onclick="return false;"  href="${contextPath}/showHistory?page=${page+1}&teamId=${team.teamId}">&gt;&gt;</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="${contextPath}/showHistory?page=${page+1}&teamId=${team.teamId}">&gt;&gt;</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</body>
</html>
