<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="admin.team.add_new_team"/></title>
</head>
<body>
<div class="container">
    <form:form method="POST" modelAttribute="team" class="form-signin"
               action="${contextPath}/createTeam">
        <h2 class="form-signin-heading text-center"><spring:message code="admin.team.add_new_team"/></h2>
        <form:input path="name" class="form-control" autofocus="true"/>
        <form:errors path="name" cssClass="has-error text-center"></form:errors>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
            code="admin.match.create"/></button>
    </form:form>
</div>
</body>
</html>
