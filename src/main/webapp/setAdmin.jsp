<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="admin.change_role"/></title>
</head>
<body>
<div class="container">
    <form:form method="POST" modelAttribute="user" class="form-signin"
               action="${contextPath}/setAdmin">
        <h2 class="form-signin-heading"><spring:message code="admin.change_role"/></h2>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:radiobuttons element="span class='radio-betraven'"  path="roles" class="form-check" items="${roles}" itemLabel="name"/>
        </div>
        <form:hidden path="username" id="username"/>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
            code="admin.change_role"/></button>
    </form:form>
</div>
</body>
</html>
