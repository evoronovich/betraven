<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="user.login"/> </title>
</head>
<body>
<div class="container">
    <form method="POST" action="${contextPath}/login" class="form-signin">
        <h2 class="form-heading"><spring:message code="user.login"/></h2>
        <div class="form-group ${error != null ? 'has-error' : 'welcome'}">
            <span>${message}</span>
            <input name="username" type="text" class="form-control" autofocus="true" placeholder=
            <spring:message code="user.username"/>>
            <input name="password" type="password" class="form-control" placeholder=
            <spring:message code="user.password"/>>
                <span>${error}</span>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
                code="user.login"/></button>
            <h4 class="text-center"><a href="${contextPath}/registration"><spring:message
                code="user.registration"/> </a></h4>
        </div>
    </form>
</div>
</body>
</html>
