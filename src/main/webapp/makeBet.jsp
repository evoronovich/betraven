<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="bet.make"/></title>
</head>
<body>
<div class="container">
    <form:form method="POST" modelAttribute="bet" class="form-signin"
               action="${contextPath}/makeBet">
        <h3 class=" text-center">
                ${bet.ratio.game.ratios[0].team} [${bet.ratio.game.ratios[0].value}]
            vs ${bet.ratio.game.ratios[1].team} [${bet.ratio.game.ratios[1].value}]
        </h3>
        <h3 class=" text-center"><fmt:formatDate
            value="${bet.ratio.game.gameTime}" pattern="dd.MM HH:mm"></fmt:formatDate></h3>
        <h3 class="text-center result-win">${bet.ratio.team} [${bet.ratio.value}] </h3>
        <form:input type="number" min="1" max="${user.account.money}" step="0.01" path="betAmount" class="form-control"
                    placeholder="betAmount"
                    autofocus="true" required="true"></form:input>
        <form:errors path="betAmount" cssClass="has-error text-center"></form:errors>
        <form:hidden path="ratio" id="ratio"></form:hidden>
        <form:hidden path="game" id="game"></form:hidden>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
            code="bet"/></button>
    </form:form>
</div>
</body>
</html>
