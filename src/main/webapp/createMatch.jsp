<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="admin.match.create.new_match"/></title>
</head>
<body>
<div class="container">
    <form:form method="POST" modelAttribute="gameForm" class="form-signin"
               action="${contextPath}/createMatch">
        <h2 class="form-signin-heading text-center"><spring:message code="admin.match.create.new_match"/></h2>
        <c:forEach begin="0" end="1" varStatus="st">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="number" min="1.01" step="0.01" path="ratios[${st.index}].value"
                            class="form-control" placeholder="<ratio>"/>
                <form:errors path="ratios[${st.index}].value" cssClass="has-error text-center"></form:errors>
            </div>
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:select items="${teams}" path="ratios[${st.index}].team" class="form-control"
                             itemLabel="name"/>
                <form:errors path="" cssClass="has-error text-center"></form:errors>
            </div>
        </c:forEach>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:input type="datetime-local" path="gameTime" class="form-control" required="true"/>
            <form:errors path="gameTime" cssClass="has-error text-center"></form:errors>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
            code="admin.match.create"/></button>
        <button class="btn btn-lg btn-primary btn-block" type="submit"
                formaction="${contextPath}/setAutoRatio" formmethod="GET"><spring:message
            code="admin.match.set_auto_ratio"/></button>
    </form:form>
</div>
</body>
</html>
