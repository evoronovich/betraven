<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet">
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="admin.match.choose_team"/> </title>
</head>
<body>
<div class="container">
    <form:form method="GET" modelAttribute="team" class="form-signin"
               action="${contextPath}/showHistory">
        <h2 class="form-signin-heading text-center"><spring:message code="admin.match.choose_team"/></h2>
        <div class="form-group ${status.error ? 'has-error' : ''}">
            <form:select items="${teams}" class="form-control" itemLabel="name" path="teamId"
                         itemValue="teamId"/>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
            code="team.choose"/></button>
    </form:form>
</div>
</body>
</html>
