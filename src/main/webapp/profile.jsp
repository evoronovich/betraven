<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="user.profile"/> </title>
</head>
<body>
    <table class="table table-nonfluid text-center">
    <thead>
    <tr>
        <th><spring:message code="bet.id"/></th>
        <th><spring:message code="bet.call"/>&nbsp[<spring:message code="team.ratio"/>]</th>
        <th><spring:message code="bet.amount"/>&nbsp[<spring:message code="bet.amount.possible_win"/>]</th>
        <th><spring:message code="game"/></th>
        <th><spring:message code="result"/></th>
    </tr>
    </thead>
    <c:forEach items="${bets}" var="bet">
        <tr>
            <td>${bet.betId}</td>
            <td>${bet.ratio.team}&nbsp[${bet.ratio.value}]</td>
            <td>${bet.betAmount}&nbsp[${bet.betAmount*bet.ratio.value}]</td>
            <td>${bet.ratio.game.ratios[0].team} vs ${bet.ratio.game.ratios[1].team}&nbsp <fmt:formatDate value="${bet.ratio.game.gameTime}"
                                                                                                     pattern="dd.MM HH:mm"></fmt:formatDate></td>
            <td>
                <c:choose>
                    <c:when test="${empty bet.ratio.game.result}">
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${bet.ratio.game.result.winner eq bet.ratio.team}">
                                <span class="result-win">${bet.betAmount*bet.ratio.value}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="result-loose">${bet.betAmount}</span>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </c:forEach>
</table>
<div class="text-center">
    <ul class="pagination">
        <c:choose>
            <c:when test="${page eq 0}">
                <li class="disabled"><a onclick="return false;" href="${contextPath}/profile?page=${page-1}">&lt;&lt;</a></li>
            </c:when>
            <c:otherwise>
                <li><a href="${contextPath}/profile?page=${page-1}">&lt;&lt;</a></li>
            </c:otherwise>
        </c:choose>
        <c:forEach begin="1" end="${maxPages}" var="i">
            <c:choose>
                <c:when test="${page eq i-1}">
                    <li class="active"><a href="${contextPath}/profile?page=${i-1}">${i}</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="${contextPath}/profile?page=${i-1}">${i}</a></li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:choose>
            <c:when test="${page eq maxPages-1}">
                <li class="disabled"><a onclick="return false;" href="${contextPath}/profile?page=${page+1}">&gt;&gt;</a></li>
            </c:when>
            <c:otherwise>
                <li><a href="${contextPath}/profile?page=${page+1}">&gt;&gt;</a></li>
            </c:otherwise>
        </c:choose>
    </ul>
</div>
</body>
</html>
