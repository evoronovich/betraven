<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="https://fonts.googleapis.com/css?family=Jura" rel="stylesheet">
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><spring:message code="admin.home"/></title>
</head>
<body>
<div class="container">
    <h4 class="text-center"><a href="${contextPath}/createTeam"><spring:message
        code="admin.team.add_new_team"/></a>
    </h4>
    <h4 class="text-center"><a href="${contextPath}/deleteTeam"><spring:message
        code="team.delete"/> </a></h4>
    <h4 class="text-center"><a href="${contextPath}/createMatch"><spring:message
        code="admin.match.create.new_match"/> </a></h4>
    <h4 class="text-center"><a href="${contextPath}/chooseMatchToDelete"><spring:message
        code="admin.match.delete"/> </a></h4>
    <h4 class="text-center"><a href="${contextPath}/chooseMatch"><spring:message
        code="admin.match.set_results"/> </a></h4>
    <h4 class="text-center"><a href="${contextPath}/chooseUser"><spring:message
        code="admin.users"/> </a></h4>
    <h4 class="text-center"><a href="${contextPath}/cashBets"><spring:message
        code="admin.match.cash_bets"/> </a></h4>
</div>
</body>
</html>
