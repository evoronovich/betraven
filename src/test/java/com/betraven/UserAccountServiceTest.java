package com.betraven;

import com.betraven.config.CustomSuccessHandler;
import com.betraven.config.WebMvcConfiguration;
import com.betraven.config.WebSecurityConfig;
import com.betraven.domain.Account;
import com.betraven.domain.User;
import com.betraven.service.UserAccountService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {Application.class, CustomSuccessHandler.class,
    WebMvcConfiguration.class, WebSecurityConfig.class})
@Transactional
@ActiveProfiles("dev")
@Ignore
public class UserAccountServiceTest {
    @Autowired
    private UserAccountService userAccountService;
    @Test
    public void saveUserTest(){
        User user = new User();
        user.setUsername("testtest");
        user.setPassword("testtest");
        userAccountService.saveUser(user);
        Assert.assertNotNull(userAccountService.findByUserName("testtest"));
    }

    @Test
    public void getCompanyAccountTest(){
        Assert.assertTrue(userAccountService.getCompanyAccount().getUser().getUsername().equals("Company"));
    }

    @Test
    public void saveAccountTest(){
        Assert.assertNotNull(userAccountService.saveAccount(new Account()));
    }
}
