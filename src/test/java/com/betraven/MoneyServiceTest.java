package com.betraven;

import com.betraven.config.CustomSuccessHandler;
import com.betraven.config.WebMvcConfiguration;
import com.betraven.config.WebSecurityConfig;
import com.betraven.domain.Account;
import com.betraven.domain.User;
import com.betraven.service.MoneyService;
import com.betraven.service.UserAccountService;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {Application.class, CustomSuccessHandler.class,
    WebMvcConfiguration.class, WebSecurityConfig.class})
@Transactional
@ActiveProfiles("dev")
@Ignore
public class MoneyServiceTest {

    @Autowired
    private MoneyService moneyService;
    @Autowired
    private UserAccountService userAccountService;

    @Test
    public void addMoneyTest() {
        Account account = new Account();
        User user = new User();
        user.setUsername("testtest");
        user.setPassword("testtest");
        userAccountService.saveUser(user);
        user.setAccount(account);
        account.setMoney(new BigDecimal(0));
        BigDecimal moneyToAdd = new BigDecimal(5);
        moneyService.addMoney(account, moneyToAdd);
        Assert.assertTrue(userAccountService.findByUserName("testtest").getAccount().getMoney()
            .compareTo(new BigDecimal(5)) == 0);
    }

    @Test
    public void cashAccountTest() {
        Account account = new Account();
        User user = new User();
        user.setUsername("testtest");
        user.setPassword("testtest");
        userAccountService.saveUser(user);
        user.setAccount(account);
        account.setMoney(new BigDecimal(5));
        BigDecimal moneyToCash = new BigDecimal(3);
        moneyService.cashAccount(account, moneyToCash);
        Assert.assertTrue(userAccountService.findByUserName("testtest").getAccount().getMoney()
            .compareTo(new BigDecimal(2)) == 0);
    }

    @Test
    public void isEnoughFundsTest() {
        Account account = new Account();
        account.setMoney(new BigDecimal(5));
        BigDecimal moneyToTest = new BigDecimal(3);
        Assert.assertTrue(moneyService.isEnoughFunds(account,moneyToTest));
    }
}
