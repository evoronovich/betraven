package com.betraven;


import com.betraven.config.CustomSuccessHandler;
import com.betraven.config.WebMvcConfiguration;
import com.betraven.config.WebSecurityConfig;
import com.betraven.domain.Team;
import com.betraven.service.TeamService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {Application.class, CustomSuccessHandler.class,
    WebMvcConfiguration.class, WebSecurityConfig.class})
@Transactional
@ActiveProfiles("dev")
@Ignore
public class TeamServiceTest {

    @Autowired
    TeamService teamService;

    @Test
    public void createTeamTest() {
        Team team = new Team();
        team.setName("test");
        teamService.saveTeam(team);
        Assert.assertEquals(teamService.findTeamByName("test").getName(), "test");
    }

    @Test
    public void getAllTeamsTest() {
        Assert.assertTrue(teamService.getAllTeams().size() > 0);
    }

    @Test
    public void getOneTeamTest() {
        Assert.assertNotNull(teamService.findTeamById(new Long(1)));

    }
}
