package com.betraven;

import com.betraven.config.CustomSuccessHandler;
import com.betraven.config.WebMvcConfiguration;
import com.betraven.config.WebSecurityConfig;
import com.betraven.domain.Bet;
import com.betraven.domain.Game;
import com.betraven.domain.Rating;
import com.betraven.domain.Ratio;
import com.betraven.domain.Result;
import com.betraven.domain.Team;
import com.betraven.domain.User;
import com.betraven.service.GameBetService;
import com.betraven.service.UserAccountService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
@ContextConfiguration(classes = {Application.class, CustomSuccessHandler.class,
    WebMvcConfiguration.class, WebSecurityConfig.class})
@Transactional
public class GameBetServiceTest {

    @Autowired
    private GameBetService gameBetService;
    @Autowired
    private UserAccountService userAccountService;

    @Test
    @Ignore
    public void saveGameTest() {
        Game game = new Game();
        Assert.assertNotNull(gameBetService.saveGame(game).getGameId());
    }


    @Test
    @Ignore
    public void getFutureGamesTest() {
        Game game = new Game();
        Date date = new Date();
        date.setYear(date.getYear() + 1);
        game.setGameTime(date);
        gameBetService.saveGame(game);
        Assert.assertTrue(gameBetService.getFutureGames(new PageRequest(0,5)).getContent().size() > 0);
    }

    @Test
    @Ignore
    public void saveBetTest() {
        Bet bet = new Bet();
        bet.setBetAmount(new BigDecimal(0));
        User user = userAccountService.getCompanyAccount().getUser();
        Assert.assertNotNull(gameBetService.saveBet(user,bet).getBetId());

    }

    @Test
    @Ignore
    public void saveResultTest() {
        Result result = new Result();
        Assert.assertNotNull(gameBetService.saveResult(result).getResultId());
    }

    @Test
    public void calculateRatiosTest(){
        List<Ratio> ratioList = new ArrayList<>();
        for (int i = 0; i<2; i++){
            Team team = new Team();
            Rating rating = new Rating();
            if (i==0){
                rating.setValue(1);
            }
            else {
                rating.setValue(10);
            }
            team.setRating(rating);
            Ratio ratio = new Ratio();
            ratio.setTeam(team);
            ratioList.add(ratio);
        }
        List<Ratio> ratiosFromTest = gameBetService.calculateRatios(ratioList);
        Assert.assertTrue(ratiosFromTest.get(0).getValue().compareTo(new BigDecimal(10))==1);
        Assert.assertTrue(ratiosFromTest.get(1).getValue().compareTo(new BigDecimal(1))==1);
    }
}
