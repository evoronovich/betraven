
# BetRaven #

* An eSports bookmaker application. You can add money, make bets on the eSports games and cash money.
* The bets are calculated automatically every hour and administrator can run calculating manually.
* The ratios can be calculated automatically based on the team rating or administrator can fill them manually.
* The administrator can create and delete matches, teams and set results of matches.

#### Admin mode ####
* username Administrator
* password 12345678

Version 1.0

### Built with ####
* Spring Boot
* Database: MySQL
* Spring Security
* Spring MVC
* Spring Data
* Apache Tiles
* Bootstrap

Voice: +375298543875

E-mail: zhenek.zik@gmail.com

Copyright 2017 Eugene Voronovich. All rigths reserved.